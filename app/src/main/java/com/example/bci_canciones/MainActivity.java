package com.example.bci_canciones;

import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Console;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public EditText editTextTermino;
    public Button buttonBuscar;
    public TextView textViewTest;
    public ListView listViewResultados;
    public ArrayList<Cancion> cancionesEncontradas = new ArrayList<Cancion>();

    public DownloadManager downloadManager;
    public long idCola;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String accion = intent.getAction();

                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(accion)) {
                    DownloadManager.Query respuestaConsulta = new DownloadManager.Query();
                    respuestaConsulta.setFilterById(idCola);

                    /*Cursor cursor = downloadManager.query(respuestaConsulta);

                    if (cursor.moveToFirst())
                    {
                        int indiceColumna = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);

                        if (DownloadManager.STATUS_SUCCESSFUL == cursor.getInt(indiceColumna))
                        {
                            String uriString = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));


                        }
                    }*/
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        editTextTermino = (EditText) findViewById(R.id.editTextTermino);
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        textViewTest = (TextView) findViewById(R.id.textViewTest);
        listViewResultados = (ListView) findViewById(R.id.listViewResultados);

        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                String nombrePorBuscar = editTextTermino.getText().toString();

                if (nombrePorBuscar.trim().isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "No se ha especificado ningún nombre", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    if (hayConexion())
                    {
                        String terminoURL = nombrePorBuscar.replaceAll(" ", "+").toLowerCase();
                        String url = "https://itunes.apple.com/search?term=" + terminoURL + "&mediaType=music&limit=20";
                        iTunesDescargaArchivo(nombrePorBuscar, url);
                        leerJSON();
                    }
                    else
                    {

                    }


                }


            }
        });
    }

    public void iTunesDescargaArchivo(String nombrePorBuscar, String url)
    {
        DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(url);
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("songs.txt")
                .setDescription("Canciones encontradas según búsqueda");
        downloadManager.enqueue(request);
    }

    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            return true;
        } else
            return false;
    }

    public void leerJSON()
    {
        
    }
}


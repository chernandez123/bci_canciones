package com.example.bci_canciones;

public class Cancion
{
    public String nombre;
    public String artista;
    public String album;

    public Cancion(String nombre, String artista, String album)
    {
        this.nombre = nombre;
        this.artista = artista;
        this.album = album;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
}

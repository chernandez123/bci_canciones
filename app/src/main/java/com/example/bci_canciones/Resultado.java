package com.example.bci_canciones;

import java.util.ArrayList;

public class Resultado
{
    public int cantidadResultados;
    public ArrayList<Cancion> cancionesEncontradas = new ArrayList<Cancion>();

    public Resultado(int cantidadResultados, ArrayList<Cancion> cancionesEncontradas) {
        this.cantidadResultados = cantidadResultados;
        this.cancionesEncontradas = cancionesEncontradas;
    }

    public int getCantidadResultados() {
        return cantidadResultados;
    }

    public void setCantidadResultados(int cantidadResultados) {
        this.cantidadResultados = cantidadResultados;
    }

    public ArrayList<Cancion> getCancionesEncontradas() {
        return cancionesEncontradas;
    }

    public void setCancionesEncontradas(ArrayList<Cancion> cancionesEncontradas) {
        this.cancionesEncontradas = cancionesEncontradas;
    }

    public void agregarCancion(Cancion cancion)
    {
        this.cancionesEncontradas.add(cancion);
    }
}
